# README #

## Setup

- make sure you have nodejs installed
- from the terminal run: `ci/install-deps.sh`
- once that's done, you can run the server: `ci/start.sh`

you can view the dashboard at: `http://0.0.0.0:4040`